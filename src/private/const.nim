# Created by Łukasz 'SteamFox' Biel, 2016
# provided under MIT license

# MODULE Constants
const
  pi*     = 3.141592653589793
  e*      = 2.718281828459045
  sqrt2*  = 1.414213562373095
  sqrt3*  = 1.732050807568877
